﻿/*
  bug.n -- tiling window management
  Copyright (c) 2010-2019 Joshua Fuhs, joten

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  @license GNU General Public License version 3
           ../LICENSE.md or <http://www.gnu.org/licenses/>

  @version 9.1.0
*/

Bar_init(m) {
  Local appBarMsg, anyText, color, color0, GuiN, h1, h2, i, id, id0, text, text0, titleWidth, trayWndId, w, wndId, wndTitle, wndWidth, x1, x2, y1, y2

  If (SubStr(Config_barWidth, 0) = "%") {
    StringTrimRight, wndWidth, Config_barWidth, 1
    wndWidth := Round(Monitor_#%m%_width * wndWidth / 100)
  } Else
    wndWidth := Config_barWidth

  wndWidth := Round(wndWidth / mmngr1.monitors[m].scaleX)
  If (Config_verticalBarPos = "tray" And Monitor_#%m%_taskBarClass) {
    Bar_ctrlHeight := Round(Bar_ctrlHeight / mmngr1.monitors[m].scaleY)
    Bar_height := Round(Bar_height / mmngr1.monitors[m].scaleY)
  }

  Monitor_#%m%_barWidth := wndWidth
  titleWidth := wndWidth
  h1 := Bar_ctrlHeight
  x1 := Config_barItemSpacing
  x2 := wndWidth
  y1 := 0
  y2 := (Bar_ctrlHeight - Bar_textHeight) / 2
  h2 := Bar_textHeight

  ;; Create the GUI window
  wndTitle := "bug.n_BAR_" m
  GuiN := (m - 1) + 1
  Debug_logMessage("DEBUG[6] Bar_init(): Gui, " . GuiN . ": Default", 6)
  Gui, %GuiN%: Default
  Gui, Destroy
  Gui, -Caption +LabelBar_Gui +LastFound +ToolWindow
  Gui, Color, %Config_backColor_#1_#3%
  Gui, Font, c%Config_fontColor_#1_#3% s%Config_fontSize%, %Config_fontName%

  ;; Views
  Loop, % Config_viewCount {
    w := Bar_getTextWidth(Config_viewNames_#%A_Index%)
    Bar_addElement(m, "view_#" A_Index, " " Config_viewNames_#%A_Index%, x1, y1, w, Config_backColor_#1_#1, Config_foreColor_#1_#1, Config_fontColor_#1_#1)
    titleWidth -= w
    x1 += w + Config_barItemSpacing
  }
  ;; Layout
  w := Bar_getTextWidth("??????")
  Bar_addElement(m, "layout", "??????", x1, y1, w, Config_backColor_#1_#2, Config_foreColor_#1_#2, Config_fontColor_#1_#2)
  titleWidth -= w
  x1 += w + Config_barItemSpacing

  ;; The x-position and width of the sub-windows right of the window title are set from the right.
  ;; <view>;<layout>;<title>;<shebang>;<time>;<date>;<anyText>;<battery>;<volumeLevel>
  ;color := "4"
  ;id    := "shebang"
  ;text  := " #! "
  color := ""
  id    := ""
  text  := ""
  If Config_readinTime {
    color .= "4"
    id    .= "time"
    text  .= "" . Config_readinTimeFormat
  }
  If (Config_readinDate And !Config_combineDateAndTime) {
    color .= ";5"
    id    .= ";date"
    text  .= ";" . Config_readinDateFormat
  }
  If Config_readinVolume {
    color .= ";9"
    id    .= ";volume"
    text  .= ";???%"
  }
  If Config_readinRam {
    color .= ";6"
    id    .= ";ram"
    text  .= ";" . ResourceMonitor_getRamText()
  }
  If Config_readinCpu {
    color .= ";7"
    id    .= ";cpu"
    text  .= ";" . ResourceMonitor_getCpuText()
  }
  If Config_readinBat {
    color .= ";8"
    id    .= ";battery"
    text  .= ";???%"
  }
  If Config_readinMusic {
    color .= ";10"
    id    .= ";music"
    text  .= ";" . MusicBee_GetNowPlaying()
  }
  StringSplit, color, color, `;
  StringSplit, id, id, `;
  StringSplit, text, text, `;
  Loop, % id0 {
    elemId := id%A_Index%
    If (elemId = "shebang")
      Gui, -Disabled
    w := Bar_getTextWidth(text%A_Index%)
    x2 -= w
    titleWidth -= w
    i := color%A_Index%
    iconIndex := i - 3
    icon := Config_barIcon_#%iconIndex%
    iconId := % elemId "_icon"
    Bar_addElement(m, elemId, text%A_Index%, x2, y1, wndWidth, Config_backColor_#1_#%i%, Config_foreColor_#1_#%i%, Config_fontColor_#1_#%i%)
    Bar_addElement(m, iconId, icon, x2, y1, wndWidth, Config_backColor_#1_#%i%, Config_foreColor_#1_#%i%, Config_fontColor_#1_#%i%, Config_iconFontSize, Config_iconFontName)
    GuiControl, -Center, Bar_#%m%_%elemId%
  }

  ;; Window title (remaining space)
  If Not Config_singleRowBar {
    titleWidth := wndWidth
    x1 := 0
    y1 += h1
    y2 += h1
  }
  Bar_addElement(m, "title", "", x1, y1, titleWidth, Config_backColor_#1_#3, Config_foreColor_#1_#3, Config_fontColor_#1_#3)

  If (Config_horizontalBarPos = "left")
    x1 := 0
  Else If (Config_horizontalBarPos = "right")
    x1 := Monitor_#%m%_width - wndWidth * mmngr1.monitors[m].scaleX
  Else If (Config_horizontalBarPos = "center")
    x1 := (Monitor_#%m%_width - wndWidth * mmngr1.monitors[m].scaleX) / 2
  Else If (Config_horizontalBarPos >= 0)
    x1 := Config_horizontalBarPos
  Else If (Config_horizontalBarPos < 0)
    x1 := Monitor_#%m%_width - wndWidth * mmngr1.monitors[m].scaleX + Config_horizontalBarPos
  If Not (Config_verticalBarPos = "tray" And Monitor_#%m%_taskBarClass)
    x1 += Monitor_#%m%_x
  x1 := Round(x1)

  Bar_#%m%_titleWidth := titleWidth
  Monitor_#%m%_barX := x1
  y1 := Monitor_#%m%_barY

  If Monitor_#%m%_showBar
    Gui, Show, NoActivate x%x1% y%y1% w%wndWidth% h%Bar_height%, %wndTitle%
  Else
    Gui, Show, NoActivate Hide x%x1% y%y1% w%wndWidth% h%Bar_height%, %wndTitle%
  WinSet, Transparent, %Config_barTransparency%, %wndTitle%
  wndId := WinExist(wndTitle)
  Bar_appBarData := ""
  If (Config_verticalBarPos = "tray" And Monitor_#%m%_taskBarClass) {
    trayWndId := WinExist("ahk_class " Monitor_#%m%_taskBarClass)
    DllCall("SetParent", "UInt", wndId, "UInt", trayWndId)
  } Else {
    appBarMsg := DllCall("RegisterWindowMessage", Str, "AppBarMsg")

    ;; appBarData: http://msdn2.microsoft.com/en-us/library/ms538008.aspx
    VarSetCapacity(Bar_appBarData, 36, 0)
    offset := NumPut(             36, Bar_appBarData)
    offset := NumPut(          wndId, offset+0)
    offset := NumPut(      appBarMsg, offset+0)
    offset := NumPut(              1, offset+0)
    offset := NumPut(             x1, offset+0)
    offset := NumPut(             y1, offset+0)
    offset := NumPut(  x1 + wndWidth, offset+0)
    offset := NumPut(y1 + Bar_height, offset+0)
    offset := NumPut(              1, offset+0)

    DllCall("Shell32.dll\SHAppBarMessage", "UInt", (ABM_NEW := 0x0)     , "UInt", &Bar_appBarData)
    DllCall("Shell32.dll\SHAppBarMessage", "UInt", (ABM_QUERYPOS := 0x2), "UInt", &Bar_appBarData)
    DllCall("Shell32.dll\SHAppBarMessage", "UInt", (ABM_SETPOS := 0x3)  , "UInt", &Bar_appBarData)
    ;; SKAN: Crazy Scripting : Quick Launcher for Portable Apps (http://www.autohotkey.com/forum/topic22398.html)
  }

  Bar_BinaryClockChars := ["⠀","⠈","⠐","⠘","⠠","⠨","⠰","⠸","⢀","⢈","⠁","⠉","⠑","⠙","⠡","⠩","⠱","⠹","⢁","⢉","⠂","⠊","⠒","⠚","⠢","⠪","⠲","⠺","⢂","⢊","⠃","⠋","⠓","⠛","⠣","⠫","⠳","⠻","⢃","⢋","⠄","⠌","⠔","⠜","⠤","⠬","⠴","⠼","⢄","⢌","⠅","⠍","⠕","⠝","⠥","⠭","⠵","⠽","⢅","⢍"]
  Bar_TimeDateState := 0
}

Bar_initCmdGui()
{
  Global Bar_#0_#0, Bar_#0_#0H, Bar_#0_#0W, Bar_#0_#1, Bar_cmdGuiIsVisible, Config_barCommands, Config_fontName, Config_fontSize
  Global Config_backColor_#1_#3, Config_fontColor_#1_#3, Config_foreColor_#1_#3

  Bar_#0_#0 := ""
  Bar_cmdGuiIsVisible := False
  wndTitle := "bug.n_BAR_0"
  Gui, 99: Default
  Gui, +LabelBar_cmdGui
  IfWinExist, %wndTitle%
    Gui, Destroy
  Gui, +LastFound -Caption +ToolWindow +AlwaysOnTop +Delimiter`;
  Gui, Color, Default
  Gui, Font, s%Config_fontSize%, %Config_fontName%
  StringSplit, cmd, Config_barCommands, `;
  Gui, Add, ComboBox, x10 y10 r%cmd0% w300 Background%Config_backColor_#1_#3% c%Config_fontColor_#1_#3% Simple vBar_#0_#0 gBar_cmdGuiEnter, % Config_barCommands
  Gui, Add, Edit, Y0 w300 Hidden vBar_#0_#1 gBar_cmdGuiEnter
  Gui, Add, Button, Y0 Hidden Default gBar_cmdGuiEnter, OK
  GuiControlGet, Bar_#0_#0, Pos
  Bar_#0_#0H += 20
  Bar_#0_#0W += 20
  Gui, Show, Hide w%Bar_#0_#0W% h%Bar_#0_#0H%, %wndTitle%
}

Bar_addElement(m, id, text, x, y1, width, backColor, foreColor, fontColor, fontSize="", fontName="") {
  Local y2

  if (fontSize == "") {
    fontSize = %Config_fontSize%
  }
  if (fontName == "") {
    fontName = %Config_fontName%
  }

  y2 := y1 + (Bar_ctrlHeight - Bar_textHeight) / 2

  Gui, Add, Text, x%x% y%y1% w%width% h%Bar_ctrlHeight% BackgroundTrans vBar_#%m%_%id%_event gBar_GuiClick,
  ;Gui, Add, Progress, x%x% y%y1% w%width% h%Bar_ctrlHeight% Background%backColor% c%foreColor% vBar_#%m%_%id%_highlighted
  ;GuiControl, , Bar_#%m%_%id%_highlighted, 100
  Gui, Font, c%fontColor% s%fontSize%, %fontName%
  Gui, Add, Text, x%x% y%y2% w%width% h%Bar_textHeight% BackgroundTrans Center vBar_#%m%_%id% HwndBar_#%m%_%id%_hwnd, %text%
}

Bar_cmdGuiEnter:
  If (A_GuiControl = "OK") Or (A_GuiControl = "Bar_#0_#0" And A_GuiControlEvent = "DoubleClick") {
    Gui, Submit, NoHide
    Bar_cmdGuiIsVisible := False
    Gui, Cancel
    WinActivate, ahk_id %Bar_aWndId%
    Main_evalCommand(Bar_#0_#0)
    Bar_#0_#0 := ""
  } Else If (A_GuiControl = "Bar_#0_#1") {
    Gui, Submit, NoHide
    Debug_logMessage("DEBUG[6] Bar_cmdGuiEnter; command: " . Bar_#0_#1, 6)
    Loop, Parse, Bar_#0_#1, `n, `r
      Main_evalCommand(A_LoopField)
  }
Return

Bar_cmdGuiEscape:
  Bar_cmdGuiIsVisible := False
  Gui, Cancel
  WinActivate, ahk_id %Bar_aWndId%
Return

Bar_getHeight()
{
  Global Bar_#0_#1, Bar_#0_#1H, Bar_#0_#2, Bar_#0_#2H, Bar_ctrlHeight, Bar_height, Bar_textHeight
  Global Config_fontName, Config_fontSize, Config_singleRowBar, Config_spaciousBar, Config_verticalBarPos

  wndTitle := "bug.n_BAR_0"
  Gui, 99: Default
  Gui, Font, s%Config_fontSize%, %Config_fontName%
  Gui, Add, Text, x0 y0 vBar_#0_#1, |
  GuiControlGet, Bar_#0_#1, Pos
  Bar_textHeight := Bar_#0_#1H
  If Config_spaciousBar
  {
    Gui, Add, ComboBox, r9 x0 y0 vBar_#0_#2, |
    GuiControlGet, Bar_#0_#2, Pos
    Bar_ctrlHeight := Bar_#0_#2H
  }
  Else
    Bar_ctrlHeight := Bar_textHeight
  Gui, Destroy

  Bar_height := Bar_ctrlHeight
  If Not Config_singleRowBar
    Bar_height *= 2
  If (Config_verticalBarPos = "tray")
  {
    WinGetPos, , , , buttonH, Start ahk_class Button
    WinGetPos, , , , barH, ahk_class Shell_TrayWnd
    If WinExist("Start ahk_class Button") And (buttonH < barH)
      Bar_height := buttonH
    Else
      Bar_height := barH
    Bar_ctrlHeight := Bar_height
    If Not Config_singleRowBar
      Bar_ctrlHeight := Bar_height / 2
  }
}

Bar_getTextWidth(x, reverse=False)
{
  Global Config_fontSize

  If reverse
  {    ;; 'reverse' calculates the number of characters to a given width.
    w := x
    i := w / (Config_fontSize - 1)
    If (Config_fontSize = 7 Or (Config_fontSize > 8 And Config_fontSize < 13))
      i := w / (Config_fontSize - 2)
    Else If (Config_fontSize > 12 And Config_fontSize < 18)
      i := w / (Config_fontSize - 3)
    Else If (Config_fontSize > 17)
      i := w / (Config_fontSize - 4)
    textWidth := i
  }
  Else
  {    ;; 'else' calculates the width to a given string.
    textWidth := StrLen(x) * (Config_fontSize - 1)
    If (Config_fontSize = 7 Or (Config_fontSize > 8 And Config_fontSize < 13))
      textWidth := StrLen(x) * (Config_fontSize - 2)
    Else If (Config_fontSize > 12 And Config_fontSize < 18)
      textWidth := StrLen(x) * (Config_fontSize - 3)
    Else If (Config_fontSize > 17)
      textWidth := StrLen(x) * (Config_fontSize - 4)
  }

  Return, textWidth
}

Bar_getTrueTextWidth(controlHwnd, newText) {
    dc := DllCall("GetDC", "Ptr", controlHwnd)

    ; 0x31 = WM_GETFONT
    SendMessage 0x31,,,, ahk_id %controlHwnd%
    hFont := ErrorLevel
    oldFont := 0
    if (hFont != "FAIL")
        oldFont := DllCall("SelectObject", "Ptr", dc, "Ptr", hFont)

    VarSetCapacity(rect, 16, 0)
    ; 0x440 = DT_CALCRECT | DT_EXPANDTABS
    h := DllCall("DrawText", "Ptr", dc, "Ptr", &newText, "Int", -1, "Ptr", &rect, "UInt", 0x440)
    ; width = rect.right - rect.left
    w := NumGet(rect, 8, "Int") - NumGet(rect, 0, "Int")

    if oldFont
        DllCall("SelectObject", "Ptr", dc, "Ptr", oldFont)
    DllCall("ReleaseDC", "Ptr", controlHwnd, "Ptr", dc)

    return w
}

Bar_GuiClick:
  Manager_winActivate(Bar_aWndId)
  If (A_GuiEvent = "Normal") {
    ;If (SubStr(A_GuiControl, -13) = "_shebang_event") {
    ;  If Not Bar_cmdGuiIsVisible
    ;    If Not (SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6) = Manager_aMonitor)
    ;      Manager_activateMonitor(SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6))
    ;  Bar_toggleCommandGui()
    ;} Else
    If (SubStr(A_GuiControl, -10) = "_time_event") {
      If (Config_combineDateAndTime) {
        If (Bar_TimeDateState == 0) {
          Bar_TimeDateState := 1
        } Else {
          Bar_TimeDateState := 0
        }
      }
    } Else {
      If Not (SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6) = Manager_aMonitor)
        Manager_activateMonitor(SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6))
      If (SubStr(A_GuiControl, -12) = "_layout_event")
        View_setLayout(-1)
      Else If InStr(A_GuiControl, "_view_#") And (SubStr(A_GuiControl, -5) = "_event")
        Monitor_activateView(SubStr(A_GuiControl, InStr(A_GuiControl, "_view_#", False, 0) + 7, 1))
    }
  }
Return

Bar_GuiContextMenu:
  Manager_winActivate(Bar_aWndId)
  If (A_GuiEvent = "RightClick") {
    If (SubStr(A_GuiControl, -12) = "_layout_event") {
      If Not (SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6) = Manager_aMonitor)
        Manager_activateMonitor(SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6))
      View_setLayout(0, +1)
    } Else If InStr(A_GuiControl, "_view_#") And (SubStr(A_GuiControl, -5) = "_event") {
      If Not (SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6) = Manager_aMonitor)
        Manager_setWindowMonitor(SubStr(A_GuiControl, 6, InStr(A_GuiControl, "_", False, 6) - 6))
      Monitor_setWindowTag(SubStr(A_GuiControl, InStr(A_GuiControl, "_view_#", False, 0) + 7, 1))
    }
  }
Return

Bar_loop:
  Bar_updateStatus()
Return

Bar_move(m)
{
  Local wndTitle, x, y

  x := Monitor_#%m%_barX
  y := Monitor_#%m%_barY

  wndTitle := "bug.n_BAR_" m
  WinMove, %wndTitle%, , %x%, %y%
}

Bar_toggleCommandGui() {
  Local wndId, x, y

  Gui, 99: Default
  If Bar_cmdGuiIsVisible {
    Bar_cmdGuiIsVisible := False
    Gui, Cancel
    Manager_winActivate(Bar_aWndId)
  } Else {
    Bar_cmdGuiIsVisible := True

    If (Config_verticalBarPos = "tray")
      x := Monitor_#%Manager_aMonitor%_x + Monitor_#%Manager_aMonitor%_barX + Monitor_#%Manager_aMonitor%_barWidth - Bar_#0_#0W
    Else
      x := Monitor_#%Manager_aMonitor%_barX + Monitor_#%Manager_aMonitor%_barWidth - Bar_#0_#0W   ;; x := mX + (mBarX - mX) + mBarW - w

    If (Config_verticalBarPos = "top") Or (Config_verticalBarPos = "tray") And (Monitor_#%Manager_aMonitor%_taskBarPos = "top" Or Not Monitor_#%Manager_aMonitor%_taskBarClass)
      y := Monitor_#%Manager_aMonitor%_y
    Else
      y := Monitor_#%Manager_aMonitor%_y + Monitor_#%Manager_aMonitor%_height - Bar_#0_#0H

    Gui, Show
    WinGet, wndId, ID, bug.n_BAR_0
    WinMove, ahk_id %wndId%, , %x%, %y%
    Window_set(wndId, "AlwaysOnTop", "On")
    GuiControl, Focus, % Bar_#0_#0
  }
}

Bar_toggleVisibility(m)
{
  Local GuiN

  GuiN := (m - 1) + 1
  If Monitor_#%m%_showBar
  {
    If Not (GuiN = 99) Or Bar_cmdGuiIsVisible
      Gui, %GuiN%: Show
  }
  Else
    Gui, %GuiN%: Cancel
}

Bar_updateLayout(m) {
  Local aView, GuiN

  aView := Monitor_#%m%_aView_#1
  GuiN := (m - 1) + 1
  GuiControl, %GuiN%: , Bar_#%m%_layout, % View_#%m%_#%aView%_layoutSymbol
}

Bar_updateStatic(m) {
  Local GuiN

  GuiN := (m - 1) + 1
  GuiControl, %GuiN%: , Bar_#%m%_shebang, #!
}

Bar_updateStatus() {
  Local bat1, bat2, bat3, GuiN, m, mute, time, vol, hours, minutes, seconds, text, textWidth, statusWidth, iconWidth, iconY

  iconY := (Bar_ctrlHeight - Bar_textHeight) / 2 - %Config_iconFontYOffset%

  If Config_readinBat {
    ResourceMonitor_getBatteryStatus(bat1, bat2)
    bat3 := bat1
  }
  If Config_readinVolume {
    SoundGet, vol, MASTER, VOLUME
    SoundGet, mute, MASTER, MUTE
    vol := Round(vol)
  }

  Loop, % Manager_monitorCount {
    m := A_Index
    GuiN := (m - 1) + 1
    Debug_logMessage("DEBUG[6] Bar_updateStatus(): Gui, " . GuiN . ": Default", 6)
    Gui, %GuiN%: Default

    statusWidth := Monitor_#%m%_barWidth
    statusWidth -= Config_barItemSpacing

    If Config_readinTime {
      If Config_readinTimeBinary {
        FormatTime, hours,, H
        FormatTime, minutes,, m
        FormatTime, seconds,, s
        If (Config_combineDateAndTime) {
          If (Bar_TimeDateState == 0) {
            time := % Bar_BinaryClockChars[hours + 1] " " Bar_BinaryClockChars[minutes + 1] " " Bar_BinaryClockChars[seconds + 1]
          } Else {
            FormatTime, time, , % Config_readinDateFormat
          }
        } else {
          time := % Bar_BinaryClockChars[hours + 1] " " Bar_BinaryClockChars[minutes + 1] " " Bar_BinaryClockChars[seconds + 1]
        }
      } Else {
        If (Config_combineDateAndTime) {
          If (Bar_TimeDateState == 0) {
            FormatTime, time, , % Config_readinTimeFormat
          } Else {
            FormatTime, time, , % Config_readinDateFormat
          }
        } else {
          FormatTime, time, , % Config_readinTimeFormat
        }
      }
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_time_hwnd, time)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_time, % time
      GuiControl, Move, Bar_#%m%_time, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_time_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_time_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      If (Config_combineDateAndTime) {
        If (Bar_TimeDateState == 0) {
          iconWidth := Bar_getTrueTextWidth(Bar_#%m%_time_icon_hwnd, Config_barIcon_#1)
        } Else {
          iconWidth := Bar_getTrueTextWidth(Bar_#%m%_time_icon_hwnd, Config_barIcon_#2)
        }
      } Else {
        iconWidth := Bar_getTrueTextWidth(Bar_#%m%_time_icon_hwnd, Config_barIcon_#1)
      }
      statusWidth -= iconWidth

      If (Config_combineDateAndTime) {
        If (Bar_TimeDateState == 0) {
          GuiControl,, Bar_#%m%_time_icon, %Config_barIcon_#1%
        } Else {
          GuiControl,, Bar_#%m%_time_icon, %Config_barIcon_#2%
        }
      } Else {
        GuiControl,, Bar_#%m%_time_icon, %Config_barIcon_#1%
      }
      GuiControl, Move, Bar_#%m%_time_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_time_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_time_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
    If Config_readinDate And !Config_combineDateAndTime {
      FormatTime, time, , % Config_readinDateFormat
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_date_hwnd, time)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_date, % time
      GuiControl, Move, Bar_#%m%_date, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_date_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_date_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      iconWidth := Bar_getTrueTextWidth(Bar_#%m%_date_icon_hwnd, Config_barIcon_#2)
      statusWidth -= iconWidth

      GuiControl,, Bar_#%m%_date_icon, %Config_barIcon_#2%
      GuiControl, Move, Bar_#%m%_date_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_date_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_date_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
    If Config_readinVolume {
      If (mute = "On") {
        ;; Change the color, if the mute is on
        ;GuiControl, +Background%Config_backColor_#1_#9% +c%Config_foreColor_#1_#9%, Bar_#%m%_volume_highlighted
        GuiControl, +c%Config_fontColor_#2_#9%, Bar_#%m%_volume
        GuiControl, +c%Config_fontColor_#2_#9%, Bar_#%m%_volume_icon
      } Else {
        ;GuiControl, +Background%Config_backColor_#2_#9% +c%Config_foreColor_#2_#9%, Bar_#%m%_volume_highlighted
        GuiControl, +c%Config_fontColor_#1_#9%, Bar_#%m%_volume
        GuiControl, +c%Config_fontColor_#1_#9%, Bar_#%m%_volume_icon
      }

      text := % vol "%"
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_volume_hwnd, text)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_volume, % text
      GuiControl, Move, Bar_#%m%_volume, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_volume_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_volume_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      iconWidth := Bar_getTrueTextWidth(Bar_#%m%_volume_icon_hwnd, Config_barIcon_#6)
      statusWidth -= iconWidth

      GuiControl,, Bar_#%m%_volume_icon, %Config_barIcon_#6%
      GuiControl, Move, Bar_#%m%_volume_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_volume_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_volume_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
    If Config_readinRam {
      text := ResourceMonitor_getRamText()
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_ram_hwnd, text)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_ram, % text
      GuiControl, Move, Bar_#%m%_ram, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_ram_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_ram_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      iconWidth := Bar_getTrueTextWidth(Bar_#%m%_ram_icon_hwnd, Config_barIcon_#3)
      statusWidth -= iconWidth

      GuiControl,, Bar_#%m%_ram_icon, %Config_barIcon_#3%
      GuiControl, Move, Bar_#%m%_ram_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_ram_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_ram_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
    If Config_readinCpu {
      text := ResourceMonitor_getCpuText()
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_cpu_hwnd, text)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_cpu, % text
      GuiControl, Move, Bar_#%m%_cpu, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_cpu_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_cpu_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      iconWidth := Bar_getTrueTextWidth(Bar_#%m%_cpu_icon_hwnd, Config_barIcon_#4)
      statusWidth -= iconWidth

      GuiControl,, Bar_#%m%_cpu_icon, %Config_barIcon_#4%
      GuiControl, Move, Bar_#%m%_cpu_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_cpu_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_cpu_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
    If Config_readinBat {
      If (bat1 < 10) And (bat2 = "off") {
        ;; Change the color, if the battery level is below 10%
        ;GuiControl, +Background%Config_backColor_#3_#8% +c%Config_foreColor_#3_#8%, Bar_#%m%_battery_highlighted
        GuiControl, +c%Config_fontColor_#3_#8%, Bar_#%m%_battery
      } Else If (bat2 = "off") {
        ;; Change the color, if the pc is not plugged in
        ;GuiControl, +Background%Config_backColor_#2_#8% +c%Config_foreColor_#2_#8%, Bar_#%m%_battery_highlighted
        GuiControl, +c%Config_fontColor_#2_#8%, Bar_#%m%_battery
      } Else {
        ;GuiControl, +Background%Config_backColor_#1_#8% +c%Config_foreColor_#1_#8%, Bar_#%m%_battery_highlighted
        GuiControl, +c%Config_fontColor_#1_#8%, Bar_#%m%_battery
      }

      text := % bat3 "%"
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_battery_hwnd, text)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_battery, % text
      GuiControl, Move, Bar_#%m%_battery, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_battery_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_battery_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      iconWidth := Bar_getTrueTextWidth(Bar_#%m%_battery_icon_hwnd, Config_barIcon_#5)
      statusWidth -= iconWidth

      GuiControl,, Bar_#%m%_battery_icon, %Config_barIcon_#5%
      GuiControl, Move, Bar_#%m%_battery_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_battery_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_battery_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
    If Config_readinMusic {
      text := MusicBee_GetNowPlaying()
      textWidth := Bar_getTrueTextWidth(Bar_#%m%_music_hwnd, text)
      statusWidth -= textWidth

      GuiControl,, Bar_#%m%_music, % text
      GuiControl, Move, Bar_#%m%_music, % "w" textWidth " x" statusWidth
      GuiControl, Move, Bar_#%m%_music_event, % "w" textWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_music_highlighted, % "w" textWidth " x" statusWidth

      statusWidth -= Config_barIconSpacing

      if (text == "") {
        iconWidth := Bar_getTrueTextWidth(Bar_#%m%_music_icon_hwnd, "")
      } else {
        iconWidth := Bar_getTrueTextWidth(Bar_#%m%_music_icon_hwnd, Config_barIcon_#7)
      }
      statusWidth -= iconWidth

      GuiControl,, Bar_#%m%_music_icon, %Config_barIcon_#7%
      GuiControl, Move, Bar_#%m%_music_icon, % "w" iconWidth " x" statusWidth " h" Bar_ctrlHeight " y" iconY
      GuiControl, Move, Bar_#%m%_music_icon_event, % "w" iconWidth " x" statusWidth
      ;GuiControl, Move, Bar_#%m%_music_icon_highlighted, % "w" iconWidth " x" statusWidth

      statusWidth -= Config_barItemSpacing
    }
  }
}

Bar_updateTitle() {
  Local aWndId, aWndTitle, content, GuiN, i, title, titleWidth, barWidth, titleX

  WinGet, aWndId, ID, A
  WinGetTitle, aWndTitle, ahk_id %aWndId%
  If InStr(Bar_hideTitleWndIds, aWndId ";") Or (aWndTitle = "bug.n_BAR_0")
    aWndTitle := ""
  If aWndId And InStr(Manager_managedWndIds, aWndId . ";") And Window_#%aWndId%_isFloating
    aWndTitle := "~ " aWndTitle
  If (Manager_monitorCount > 1)
    aWndTitle := "[" Manager_aMonitor "] " aWndTitle

  title := aWndTitle

  if (StrLen(title) > 48) {
    ;; Shorten the window title if its length exceeds the width of the bar
    title := SubStr(title, 1, 48) . "…"
  }
  StringReplace, title, title, &, &&, All     ;; Special character '&', which would underline the next letter.


  Loop, % Manager_monitorCount {
    barWidth := Monitor_#%A_Index%_barWidth
    titleWidth := Bar_getTrueTextWidth(Bar_#%A_Index%_title_hwnd, title)
    titleX := barWidth / 2 - titleWidth / 2

    GuiN := (A_Index - 1) + 1
    Debug_logMessage("DEBUG[6] Bar_updateTitle(): Gui, " . GuiN . ": Default", 6)
    Gui, %GuiN%: Default
    GuiControlGet, content, , Bar_#%A_Index%_title
    If (A_Index = Manager_aMonitor) {
      If Not (content = title)
        GuiControl,, Bar_#%A_Index%_title, % title
        GuiControl, Move, Bar_#%A_Index%_title, % "w" titleWidth "x" titleX
        GuiControl, Move, Bar_#%A_Index%_title_event, % "w" titleWidth "x" titleX
        GuiControl, Move, Bar_#%A_Index%_title_highlighted, % "w" titleWidth "x" titleX
    } Else If Not (content = "")
      GuiControl, , Bar_#%A_Index%_title,
  }
  Bar_aWndId := aWndId
}

Bar_updateView(m, v) {
  Local managedWndId0, wndId0, wndIds

  GuiN := (m - 1) + 1
  Gui, %GuiN%: Default
  Debug_logMessage("DEBUG[6] Bar_updateView(): m: " . m . "; Gui, " . GuiN . ": Default", 6)

  StringTrimRight, wndIds, Manager_managedWndIds, 1
  StringSplit, managedWndId, wndIds, `;

  If (v = Monitor_#%m%_aView_#1) {
    ;; Set foreground/background colors if the view is the current view.
    ;GuiControl, +Background%Config_backColor_#2_#1% +c%Config_foreColor_#2_#1%, Bar_#%m%_view_#%v%_highlighted
    GuiControl, +c%Config_fontColor_#2_#1%, Bar_#%m%_view_#%v%
  } Else {
    ;; Set foreground/background colors.
    ;GuiControl, +Background%Config_backColor_#1_#1% +c%Config_foreColor_#1_#1%, Bar_#%m%_view_#%v%_highlighted
    GuiControl, +c%Config_fontColor_#1_#1%, Bar_#%m%_view_#%v%
  }

  Loop, % Config_viewCount {
    StringTrimRight, wndIds, View_#%m%_#%A_Index%_wndIds, 1
    StringSplit, wndId, wndIds, `;
    TagPercent := wndId0 / managedWndId0 * 100
    If (TagPercent > 0 and v != A_Index) {
      GuiControl, +c%Config_fontColor_#3_#1%, Bar_#%m%_view_#%A_Index%
    }
    ;GuiControl, , Bar_#%m%_view_#%A_Index%_highlighted, % wndId0 / managedWndId0 * 100    ;; Update the percentage fill for the view.
    GuiControl, , Bar_#%m%_view_#%A_Index%, % Config_viewNames_#%A_Index%                 ;; Refresh the number on the bar.
  }
}
