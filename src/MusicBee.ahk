MusicBee_FormatTime(time) {
    seconds := time / 1000
    hours := Floor(seconds / 3600)
    seconds := Mod(seconds, 3600)
    minutes := Floor(seconds / 60)
    seconds := Mod(seconds, 60)
    seconds := Floor(seconds)

    if (seconds < 10) {
        seconds := % "0" seconds
    }
    if (minutes < 10) {
        minutes := % "0" minutes
    }

    if (hours != 0) {
        return % hours ":" minutes ":" seconds
    } else {
        return % minutes ":" seconds
    }
}

MusicBee_GetNowPlaying() {
    detectHidden := A_DetectHiddenWindows
    DetectHiddenWindows, On
    ipcId := WinExist("MusicBee IPC Interface")

    SendMessage, 0x0400, 999, 0,, ahk_id %ipcId%
    probeState := ErrorLevel

    SendMessage, 0x0400, 109, 0,, ahk_id %ipcId%
    playState := ErrorLevel

    SendMessage, 0x0400, 110, 0,, ahk_id %ipcId%
    position := ErrorLevel

    SendMessage, 0x0400, 141, 16,, ahk_id %ipcId%
    durationData := ErrorLevel
    MBIPC_Unpack_s(MBIPC_GetLResult(durationData), displayDuration)
    SendMessage, 0x0400, 900, %durationData%,, ahk_id %ipcId%

    SendMessage, 0x0400, 142, 32,, ahk_id %ipcId%
    artistData := ErrorLevel
    MBIPC_Unpack_s(MBIPC_GetLResult(artistData), artist)
    SendMessage, 0x0400, 900, %artistData%,, ahk_id %ipcId%

    SendMessage, 0x0400, 142, 31,, ahk_id %ipcId%
    albumArtistData := ErrorLevel
    MBIPC_Unpack_s(MBIPC_GetLResult(albumArtistData), albumArtist)
    SendMessage, 0x0400, 900, %albumArtistData%,, ahk_id %ipcId%

    SendMessage, 0x0400, 142, 65,, ahk_id %ipcId%
    titleData := ErrorLevel
    MBIPC_Unpack_s(MBIPC_GetLResult(titleData), title)
    SendMessage, 0x0400, 900, %titleData%,, ahk_id %ipcId%

    displayArtist := artist
    if (StrLen(artist) > 64) {
        displayArtist := albumArtist
    }

    state := ""
    if (playState = 6) {
        state := " [paused]"
    }

    duration := 0
    splitDuration := StrSplit(displayDuration, ":")
    duration += splitDuration[1] * 60
    duration += splitDuration[2]
    duration *= 1000

    DetectHiddenWindows, %detectHidden%

    StringReplace, displayArtist, displayArtist, &, &&, All
    StringReplace, title, title, &, &&, All

    if (probeState = 1) {
        return % displayArtist " - " title " [" MusicBee_FormatTime(position) "/" MusicBee_FormatTime(duration) "]" state
    } else {
        return ""
    }
}

;;;;

MBIPC_Unpack_s(Byref lr, ByRef string_1)
{
    string_1 := ""

    mmf := MBIPC_OpenMmf(lr)
    if !mmf
        return 0

    view := MBIPC_MapMmfView(mmf, lr, ptr)
    if !view
    {
        MBIPC_CloseMmf(mmf)
        return 0
    }

    byteCount := NumGet(ptr+0, 0, "Int")
    ptr += 4

    if byteCount > 0
        string_1 := StrGet(ptr, byteCount // 2, "UTF-16")

    MBIPC_UnmapMmfView(view)

    MBIPC_CloseMmf(mmf)

    return 1
}

MBIPC_OpenMmf(ByRef lr)
{
    if !lr
        return 0

    ; FILE_MAP_READ = 0x0004 = 4
    ; FALSE = 0
    return DllCall("OpenFileMapping", UInt, 4, Int, 0, Str, "mbipc_mmf_" . NumGet(lr, 0, "UShort"), UInt)
}

MBIPC_CloseMmf(mmf)
{
    DllCall("CloseHandle", UInt, mmf)
}

MBIPC_MapMmfView(mmf, ByRef lr, ByRef ptr)
{
    ; FILE_MAP_READ = 0x0004 = 4
    view := DllCall("MapViewOfFile", UInt, mmf, UInt, 4, UInt, 0, UInt, 0, UInt, 0, UInt)

    ptr := view + NumGet(lr, 2, "UShort") + 8

    return view
}

MBIPC_UnmapMmfView(view)
{
    DllCall("UnmapViewOfFile", UInt, view)
}

MBIPC_GetLResult(el)
{
    if el = FAIL
        return 0

    VarSetCapacity(lr, 4)
    NumPut(el, lr, 0, "Int")
    return lr
}