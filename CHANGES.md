# Changes
Required by license to disclose changes.

## Bar
* Disabled shebang
* Icon support
* Time and date can be combined and clicked on to change between states
* MusicBee integration via MusicBeeIPC plugin
* Proper text resizing
* Clicking items no longer repositions mouse cursor
* Bar elements no longer have progress element
* Time has optional binary clock mode

## Config
* Renamed `Config_readinMemoryUsage` to `Config_readinRam`
* Added `Config_readinMusic`, only supports MusicBee currently
* Added `Config_readinTimeBinary` for enabling binary clock
* Removed `Config_readinDiskLoad` and `Config_readinNetworkLoad`
* Added `Config_barItemSpacing`, `Config_barIconSpacing` and `Config_iconFontYOffset`
* Added `Config_barIcons`: `<time>;<date>;<ram>;<cpu>;<battery>;<volume>;<music>`
* Added `Config_combineDateAndTime`

## ResourceMonitor
* RAM is now in MB
* Different CPU load getting functions

## Window
* Implement [#283](https://github.com/fuhsjr00/bug.n/pull/283)
